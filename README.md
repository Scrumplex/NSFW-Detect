NSFW Detect
---
Webapp to check if an image contains NSFW content, without uploading any content. 
All work is done locally in your browser.

The webapp is available at [https://scrumplex.gitlab.io/NSFW-Detect](https://scrumplex.gitlab.io/NSFW-Detect/index.html).

Original work by [Patrick Wied (nude.js)](https://github.com/pa7/nude.js).

# License
The project is licensed under [MIT License](LICENSE) unless otherwise stated.
